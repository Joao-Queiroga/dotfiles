(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("a556e4e6fc62469cd28a57c3b5386807d676a33176659f849fc53fa8763f5955" "eca44f32ae038d7a50ce9c00693b8986f4ab625d5f2b4485e20f22c47f2634ae" default))
 '(package-selected-packages
   '(visual-fill-column org-bullets forge evil-magit evil-collection evil general helpful ivy-rich all-the-icons which-key rainbow-delimiters dracula-theme use-package doom-modeline counsel command-log-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(match ((t (:background "#44475a" :foreground nil)))))
