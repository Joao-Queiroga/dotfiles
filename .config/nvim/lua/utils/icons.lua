local M = {}

M.devIcons = {
	File				  = " ",
	Module 	  	  = " ",
	Namespace 	  = " ",
	Package	  	  = " ",
	Class		  	  = " ",
	Method				= " ",
	Property      = " ",
	Field         = " ",
	Constructor	  = " ",
	Enum				  = " ",
	EnumMember    = " ",
	Interface		  = " ",
	Function		  = "󰊕 ",
	Variable		  = " ",
	Constant		  = " ",
	String			  = " ",
	Number			  = " ",
	Boolean			  = " ",
	Array				  = " ",
	Object			  = " ",
	Key					  =	" ",
	Null				  = "󰟢 ",
	Struct			  = " ",
	Event         = " ",
	Operator			= " ",
	TypeParameter = " ",
}

return M
