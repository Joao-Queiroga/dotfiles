vim.loader.enable()
require'user.options'
require'user.lazy'
require'user.keymaps'
require'user.highlights'
