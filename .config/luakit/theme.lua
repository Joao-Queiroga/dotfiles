--------------------------
-- Default luakit theme --
--------------------------

local theme = {}

-- Default settings
theme.font = "12px monospace"
theme.fg   = "#f8f8f2"
theme.bg   = "#282a36"

-- Genaral colours
theme.success_fg = "#50fa7b"
theme.loaded_fg  = "#8be9fd"
theme.error_fg = "#f8f8f2"
theme.error_bg = "#FF5555"

-- Warning colours
theme.warning_fg = "#FF5555"
theme.warning_bg = "#282a36"

-- Notification colours
theme.notif_fg = "#f8f8f2"
theme.notif_bg = "#282a36"

-- Menu colours
theme.menu_fg                   = "#f8f8f2"
theme.menu_bg                   = "#282a36"
theme.menu_selected_fg          = "#f8f8f2"
theme.menu_selected_bg          = "#44475a"
theme.menu_title_bg             = "#282a36"
theme.menu_primary_title_fg     = "#ff5555"
theme.menu_secondary_title_fg   = "#BFBFBF"

theme.menu_disabled_fg = "#E6E6E6"
theme.menu_disabled_bg = theme.menu_bg
theme.menu_enabled_fg = theme.menu_fg
theme.menu_enabled_bg = theme.menu_bg
theme.menu_active_fg = "#50fa7b"
theme.menu_active_bg = theme.menu_bg

-- Proxy manager
theme.proxy_active_menu_fg      = '#f8f8f2'
theme.proxy_active_menu_bg      = '#282a36'
theme.proxy_inactive_menu_fg    = '#E6E6E6'
theme.proxy_inactive_menu_bg    = '#282a36'

-- Statusbar specific
theme.sbar_fg         = "#f8f8f2"
theme.sbar_bg         = "#282a36"

-- Downloadbar specific
theme.dbar_fg         = "#f8f8f2"
theme.dbar_bg         = "#282a36"
theme.dbar_error_fg   = "#FF5555"

-- Input bar specific
theme.ibar_fg           = "#f8f8f2"
theme.ibar_bg           = "#282a36"

-- Tab label
theme.tab_fg            = "#f8f8f2"
theme.tab_bg            = "#282a36"
theme.tab_hover_bg      = "#44475a"
theme.tab_ntheme        = "#dddddd"
theme.selected_fg       = "#f8f8f2"
theme.selected_bg       = "#44475a"
theme.selected_ntheme   = "#dddddd"
theme.loading_fg        = "#8be9fd"
theme.loading_bg        = "#282a36"

theme.selected_private_tab_bg = "#bd93f9"
theme.private_tab_bg    = "#6272a4"

-- Trusted/untrusted ssl colours
theme.trust_fg          = "#50fa7b"
theme.notrust_fg        = "#FF5555"

-- Follow mode hints
theme.hint_font = "10px monospace, courier, sans-serif"
theme.hint_fg = "#f8f8f2"
theme.hint_bg = "#44475a"
theme.hint_border = "1px dashed #bd93f9"
theme.hint_opacity = "0.3"
theme.hint_overlay_bg = "rgba(255,255,153,0.3)"
theme.hint_overlay_border = "1px dotted #000000"
theme.hint_overlay_selected_bg = "rgba(80,250,123,0.3)"
theme.hint_overlay_selected_border = theme.hint_overlay_border

-- General colour pairings
theme.ok = { fg = "#f8f8f2", bg = "#282a36" }
theme.warn = { fg = "#FF5555", bg = "#282a36" }
theme.error = { fg = "#f8f8f2", bg = "#FF5555" }

-- Gopher page style (override defaults)
theme.gopher_light = { bg = "#E8E8E8", fg = "#17181C", link = "#8be9fd" }
theme.gopher_dark  = { bg = "#282a36", fg = "#f8f8f2", link = "#ffb86c" }

return theme

-- vim: et:sw=4:ts=8:sts=4:tw=80
