vim.loader.enable()
require 'user.options'
require 'user.keymaps'
require 'plugins'
require 'user.highlight'
