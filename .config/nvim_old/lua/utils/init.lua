local M = {}

M.source = function (module)
	return function ()
		require(module)
	end
end

M.icons = require('utils.icons')


return M
