require("lspconfig")

require('plugins.lsp.handlers').setup()
require 'plugins.lsp.mason'
require 'plugins.lsp.null-ls'
