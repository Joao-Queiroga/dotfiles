return {
	settings = {

		Lua = {
			diagnostics = {
				globals = { "vim" },
			},
			workspace = {
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.stdpath("config") .. "/lua"] = true,
					[os.getenv("HOME") .. "/.config/awesome"] = true,
					["/usr/share/awesome/lib"] = true,
					[vim.fn.expand("/nix/store/*/share/awesome/lib")] = true, -- For awesomewm libraries in nixos
				},
			},
		},
	},
}
