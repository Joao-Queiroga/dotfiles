local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

local source = require('utils').source

require("lazy").setup({
	'nvim-lua/popup.nvim',
	'nvim-lua/plenary.nvim',
	-- lazy.nvim
	{
		"folke/noice.nvim",
		opts = {
			presets = { inc_rename = true }
		},
		dependencies = {
			"MunifTanjim/nui.nvim",
			-- OPTIONAL:
			--   `nvim-notify` is only needed, if you want to use the notification view.
			--   If not available, we use `mini` as the fallback
			-- "rcarriga/nvim-notify",
		}
	},
	{
		'numToStr/Comment.nvim',
		event = "VeryLazy",
		config = true,
	},
	{
		's1n7ax/nvim-window-picker',
		name = 'window-picker',
		event = "VeryLazy",
		version = '2.*',
		opts = require'plugins.config.window-picker'
	},
	{
		'nvim-neo-tree/neo-tree.nvim',
		opts = require'plugins.config.neo-tree',
		cmd = "Neotree",
		dependencies = {
			'nvim-lua/plenary.nvim',
			'nvim-tree/nvim-web-devicons',
			'MunifTanjim/nui.nvim',
		}
	},
	{
		'lukas-reineke/indent-blankline.nvim',
		event = "VeryLazy",
		opts = require'plugins.config.indentline',
	},
	{
		'ahmedkhalf/project.nvim',
		event = "VeryLazy",
		config = source('plugins.config.project')
	},
	'christoomey/vim-tmux-navigator',
	{
		'folke/which-key.nvim',
		event = "VeryLazy",
		config = source'plugins.config.which-key'
	},

	--cmp plugins
	{
		'hrsh7th/nvim-cmp',
		event = "InsertEnter",
		dependencies = {
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-path',
			'saadparwaiz1/cmp_luasnip',
			'hrsh7th/cmp-nvim-lsp',
		},
		config = source'plugins.config.cmp'
	},

	--snippets
	{
		'L3MON4D3/Luasnip',
		dependencies = 'rafamadriz/friendly-snippets',
		event = "VeryLazy",
		config = function()
			require("luasnip/loaders/from_vscode").lazy_load()
		end
	},

	-- Lsp
	{
		'neovim/nvim-lspconfig',
		config = source'plugins.lsp',
	},
	{
		'smjonas/inc-rename.nvim',
		event = "LspAttach",
		config = true,
	},
	{
		'SmiteshP/nvim-navbuddy',
		event = "LspAttach",
		opts = require('plugins.config.navbuddy')
	},
	{
		'SmiteshP/nvim-navic',
		event = "LspAttach",
		opts = require('plugins.config.navic')
	},
	'williamboman/mason.nvim', -- simple to use language server installer
	'williamboman/mason-lspconfig.nvim', -- simple to use language server installer
	'jose-elias-alvarez/null-ls.nvim', -- LSP diagnostics and code actions
	{
		'mfussenegger/nvim-jdtls',
		ft = "java",
	},
	{
		'simrat39/rust-tools.nvim',
		ft = "rust",
		config = source("plugins.lsp.settings.rust"),
	},
	{
		'elkowar/yuck.vim',
		ft = "yuck"
	},
	{
		"nvim-neorg/neorg",
		build = ":Neorg sync-parsers",
		dependencies = { "nvim-lua/plenary.nvim" },
		opts = require("plugins.config.neorg"),
	},
	{
		'nvim-orgmode/orgmode',
		ft = "org",
		config = source('plugins.config.org')
	},
	{
		'mfussenegger/nvim-dap',
		event = "VeryLazy",
		-- config = true,
	},
	{
		'rcarriga/nvim-dap-ui',
		event = "VeryLazy",
		dependencies = { 'mfussenegger/nvim-dap' },
		enable = false,
		config = true,
	},

	-- Colors
	{
		'brenoprata10/nvim-highlight-colors',
		event = "VeryLazy",
		opts = require'plugins.config.highlight-colors',
	},

	-- Align
	{
		'echasnovski/mini.align',
		version = false,
		event = "VeryLazy",
		opts = require'plugins.config.align',
	},

	-- Telescope
	{
		'nvim-telescope/telescope.nvim',
		dependencies = {
			'nvim-lua/plenary.nvim',
			'nvim-telescope/telescope-file-browser.nvim',
			'nvim-telescope/telescope-media-files.nvim',
			'nvim-telescope/telescope-ui-select.nvim',
		},
		config = function ()
			require'plugins.config.telescope'
		end,
	},

	-- Treesitter
	{
		'nvim-treesitter/nvim-treesitter',
		event="VeryLazy",
		build = function()
			local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
			ts_update()
		end,
		config = function ()
			require'plugins.config.treesitter'
		end,
	},
	{
		'nvim-treesitter/playground',
		event="VeryLazy",
	},
	{
		'p00f/nvim-ts-rainbow',
		event="VeryLazy",
	},
	{
		'JoosepAlviste/nvim-ts-context-commentstring',
		event="VeryLazy",
	},
	{
		'windwp/nvim-autopairs',
		event="InsertEnter",
		config = function ()
			require'plugins.config.autopairs'
		end
	},
	'nvim-treesitter/nvim-treesitter-refactor',

	-- Git
	{
		'lewis6991/gitsigns.nvim',
		event = "VeryLazy",
		opts = require'plugins.config.gitsigns',
	},
	{
		'kdheepak/lazygit.nvim',
		dependencies = 'nvim-lua/plenary.nvim',
		cmd = "LazyGit",
		enabled = vim.fn.executable('lazygit')
	},

	--Lualine
	{
		'nvim-lualine/lualine.nvim',
		dependencies = { 'nvim-tree/nvim-web-devicons', lazy = true },
		opts = require'plugins.config.lualine',
	},

	--Themes
	{ 'Mofiqul/dracula.nvim',
		name = 'dracula',
		lazy = false,
		priority = 1000,
		config = function ()
			vim.cmd[[colorscheme dracula]]
		end,
	},

	{
		'akinsho/bufferline.nvim',
		event = "VeryLazy",
		opts = require'plugins.config.bufferline',
	},

	-- Terminal
	{
		'akinsho/toggleterm.nvim',
		version = '*',
		keys = "<C-t>",
		opts = require'plugins.config.toggleterm',
	},
})
