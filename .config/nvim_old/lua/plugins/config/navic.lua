return {
	icons = require('utils').icons.devIcons,
	highlight = true,
	lsp = {
		auto_attach = true
	}
}
