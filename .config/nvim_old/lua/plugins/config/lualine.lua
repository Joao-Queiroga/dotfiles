return {
	options = {
		icons_enabled = true,
		theme = 'dracula',
		component_separators = { left = '|', right = '|'},
		section_separators = { left = '', right = ''},
		disabled_filetypes = {
			statusline = {
				"help",
				"startify",
				"dashboard",
				"packer",
				"neogitstatus",
				"NvimTree",
				"Trouble",
				"alpha",
				"lir",
				"Outline",
				"spectre_panel",
				"toggleterm",
				"neo-tree",
				"neo-tree-popup",
				"notify"
			},
			winbar = {
				"help",
				"startify",
				"dashboard",
				"packer",
				"neogitstatus",
				"NvimTree",
				"Trouble",
				"alpha",
				"lir",
				"Outline",
				"spectre_panel",
				"toggleterm",
				"neo-tree",
				"neo-tree-popup",
				"notify"
			},
		},
		ignore_focus = {},
		always_divide_middle = true,
		globalstatus = false,
		refresh = {
			statusline = 1000,
			tabline = 1000,
			winbar = 500,
		}
	},
	sections = {
		lualine_a = {'mode'},
		lualine_b = {'branch', 'diff', 'diagnostics'},
		lualine_c = {'filename'},
		lualine_x = {'encoding', 'fileformat', 'filetype'},
		lualine_y = {'progress'},
		lualine_z = {'location'}
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {'filename'},
		lualine_x = {'location'},
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {},
	winbar = {
		lualine_a = {
			{
				'filetype',
				icon_only = true,
				separator = "",
				color = {bg = 'none'},
			},
			{
				'filename',
				padding = 0,
				color = { bg = 'none', fg = 'normal' },
			},
		},
		lualine_b = {
			{
				"navic",
				fmt = function (str)
					return str ~= '' and '%#NavicText#> %*' .. str or nil
				end,
			}
		},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {},
	},
	inactive_winbar = {
		lualine_a = {
			{
				'filetype',
				icon_only = true,
				separator = "",
				color = {bg = 'none'},
			},
		},
		lualine_b = {
			{
				'filename',
				color = {bg = 'none'},
				padding = 0,
			},
		},
		lualine_c = {
			{
				"navic",
				fmt = function (str)
					return str ~= '' and '%#NavicText#> %*' .. str or nil
				end
			}
		},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {},
	},
	extensions = {}
}
