local wk = require("which-key")

wk.setup()

local leader_mappings = {
	e = { "<cmd>Neotree toggle<cr>", "Toggle FileTree" },
	f = {
		name = "files",
		f = { "<cmd>Telescope find_files<cr>", "Find files" },
		r = { "<cmd>Telescope oldfiles<cr>", "Recent files" },
		b = { "<cmd>Telescope file_browser<cr>" ,"File Browser" },
	},
	c = {
		name = "code",
		a = "Code actions"
	},
	g = {
		name = "git",
		l = { "<cmd>LazyGit<cr>", "Lazygit" }
	},
	t = { "<cmd>Telescope live_grep<cr>", "Live grep" },
}

wk.register(leader_mappings, { prefix = '<leader>' })
