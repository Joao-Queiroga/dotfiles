return {
	indentLine_enabled = 1,
	char = "▏",
	filetype_exclude = {
		"help",
		"terminal",
		"dashboard",
		"lazy",
		"lspinfo",
		"mason",
		"TelescopePrompt",
		"TelescopeResults",
		"noice"
	},
	show_current_context = true,
	buftype_exclude = { "terminal" },
	show_trailing_blankline_indent = false,
	show_first_indent_level = true,
}
