return {
	autoselect_one = true,
	include_current = false,
	filter_rules = {
		-- filter using buffer options
		bo = {
			-- if the file type is one of following, the window will be ignored
			filetype = { 'neo-tree', "neo-tree-popup", "notify", "NvimTree" },

			-- if the buffer type is one of following, the window will be ignored
			buftype = { 'terminal', "quickfix" },
		},
	},
	other_win_hl_color = '#4493C8',
}
