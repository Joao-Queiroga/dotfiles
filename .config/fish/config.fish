if status is-interactive
	starship init fish | source
	if test "$fish_login" = "1"
		bass source /etc/profile
		bass source ~/.profile
	end
	fish_config theme choose "Dracula Official"
	fish_vi_key_bindings
end
function fish_greeting
    pfetch
end
